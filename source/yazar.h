//Bu�rahan Arslan
//330070
//HW-1
//yazar.h

#ifndef YAZAR_H
#define YAZAR_H



using namespace std;

class yazar {

public:

	string yazaradi; // yazarin ad�
	yazar *next;	// sonraki yazar
   class kitap *kitabi;	//class'� silince hata ald�k! include problemi olabilir
};		// kitap* kitabi ==> yazar�n eklenmi� ilk kitab�

class yazarlist {


public:
	yazar* head;

	yazarlist();
	~yazarlist();
	bool empty() const;			//listenin bo� olmas� kontrol�
	void print();				// listenin bas�lmas�
	void yazarEkle(const string& y);	//yeni yazar ekleme
	void yazarSil(); //yazar silme
	int elemanSayisi(); //ka� adet yazar oldu�unu bulma
};

int yazarlist::elemanSayisi()
{	//tum yazarlardan ge�erek her d�ng�de sayac�n artt�r�mas�
	int a = 0;
	yazar* temp = head;
	while ((temp->next) != NULL)
	{							
		temp = temp->next;
		a++;
	}
	a++;
	return a;
}

yazarlist::yazarlist() {

	head = NULL;
}
yazarlist::~yazarlist() {

	//while (!empty()) yazarSil();  >> yazarSil() Tekrar cagiriliyordu !
}

bool yazarlist::empty() const							// is list empty?
{
	return head == NULL;
}

void yazarlist::yazarEkle(const string& y) {

	yazar* v = new yazar;
	v->yazaradi = y;
	v->next = NULL;
	v->kitabi = NULL;
						//yeni yazar node'u olu�turup setlenmesi

	if (head == NULL) head = v;

	else
	{
		yazar* first = head;
		while (first->next != NULL) first = first->next;
		first->next = v;
	}

}

void yazarlist::yazarSil() {

	cout << "Kacinci yazar silinecek ? " << endl;
	int i;
	cin >> i;

					//yazar silme i�lemi

	if (i == 1)
	{
		yazar* temp = head;
		head = head->next;
		delete temp;
	}
	else if (i> (yazarlist::elemanSayisi()))
	{
		cout << "Listede bu kadar yazar yok :)" << endl;
	}
	else
	{
		yazar* temp = head;
		while (i>2)
		{
			temp = temp->next;
			i--;
		}
		yazar *temp2 = temp->next;
		temp->next = temp2->next;
		delete temp2;
	}

}

void yazarlist::print() {//Print Turleri ve B.Tarihini gosterecek 
							//sekilde duzenlenebilir.
	int i = 1;

	if (empty())
	{
		cout << "Liste bos." << endl;


		return;
	}

	yazar* first = head;

	while (first != NULL)
	{
		cout << i << ".  " << first->yazaradi << "\n" << endl;
		first = first->next;
		i++;
	}


}




#endif