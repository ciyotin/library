//Bu�rahan Arslan
//330070
//HW-1
//main.cpp

#include <iostream>
#include <string>
#include <cstdlib>
#include "kitap.h"
#include "yazar.h"
#include "sayfa.h"
#define PASSWORD 61

using namespace std;



int main() {
	yazarlist ylist;
	kitaplist klist;
	sayfalist slist;
							//kutuphaneye ilk basta kitap ve yazar eklenmesi
	ylist.yazarEkle("GRRM");
	ylist.yazarEkle("JRRT");
	ylist.yazarEkle("Omer Seyfettin");
	ylist.yazarEkle("Orhan Pamuk");
	klist.kitapEkle("Silmarillion", ylist,slist,2005);	
	slist.dosyaAc("Silmarillion");						
	klist.kitapEkle("ASOIAF", ylist, slist,2004);
	slist.dosyaAc("ASOIAF");
	klist.kitapEkle("Lord of the Rings", ylist, slist,2006);
	slist.dosyaAc("Lord of the Rings");
	system("cls");
	
	
	cout << "----------------------------" << endl;
	cout << "Kutuphaneye Hosgeldiniz." << endl;
	cout << "----------------------------" << endl;
	cout << "\n" << endl;

	int sonlanma = 0; //program�n devaml�l��� i�in de�i�ken

	while (sonlanma == 0)
	{
		cout << "Lutfen secim yapiniz." << "\n" << endl;
		cout << "1. Yonetici Girisi" << endl;
		cout << "2. Kullanici Girisi" << "\n" << endl;

		int a;
		cin >> a;
		cout << endl;


		if (a == 1) { //Yonetici Girisi
			int pswrd;
			cout << "Devam etmek icin sifreyi girin." << "\n" << endl;
			cin >> pswrd;
			system("cls");

			if (pswrd == PASSWORD) {	//Sifre dogru
				system("cls");
				cout << "Yonetici olarak giris yaptiniz." << "\n" << endl;
				int b = 0,yonetici=0;
				while (yonetici == 0)
				{
					cout << "Ne yapmak istiyorsunuz ?" << endl;
					cout << "1. Yazarlari Goruntule" << endl;
					cout << "2. Kitaplari Goruntule" << endl;
					cout << "3. Yazar Ekle" << endl;
					cout << "4. Kitap Ekle" << endl;
					cout << "5. Yazar Sil" << endl;
					cout << "6. Kitap Sil" << endl;
					cin >> b;
					cout << "------------------------" << endl;
					system("cls");


					switch (b) {

					case 1: {
						ylist.print();	//yazar listesinin s�rayla bas�lmas�
						break;
					}
					case 2: {
						klist.print();	//kitap listesinin s�rayla bas�lmas�
						break;
						
					}
					case 3: {
						string ad;
						cout << "Yazarin adini girin" << endl;
						cin.ignore(); // ==> bo�luklu string almak i�in gereken fonksiyon
						getline(cin, ad);
						ylist.yazarEkle(ad);   // yazar� ekleyen fonksiyon
						cout << "Yazar eklendi!\n" << endl;
						ylist.print(); //yazarlar�n bas�lmas�
						break;
					}

					case 4: {		
						string ad;
						int tarih;
						cout << "Kitabin adini girin" << endl;
						cin.ignore();
						getline(cin, ad);
						cout << "Basim tarihini girin" << endl;
						cin >> tarih;
						klist.kitapEkle(ad, ylist, slist, tarih); //kitap ekleme fonksiyonu
						cout << "Kitap eklendi! \n" << endl;
						klist.print(); //kitaplar� yazd�rma
						slist.dosyaAc(ad); //kitapa sayfa a�ma fonksiyonu
						break;
					}


					case 5: {
						system("cls");
						ylist.print(); //yazar listin bas�lmas�
						ylist.yazarSil(); //se�ilen yazar�n silinmesi
						ylist.print(); //yazar listin bas�lmas�
						break;
					}

					case 6: {
						system("cls");
						klist.print(); //kitap listin bas�lmas�
						klist.kitapSil(); //se�ilen kitab�n silinmesi
						klist.print(); //kitap listin bas�lmas�
						break;
					}


					default: {	
						cout << "Belirtilen seceneklerden birini secmediniz."
							<< endl;
						break;
					}
					}//endswitch
				
					cout << "Ana menu icin 1 Yonetici menusu icin 0 girin." << endl;
					cin >> yonetici;
					system("cls");
				
				}//endwhile
			}//endif

			else {  
				cout << "Yanlis parola girdiniz." << "\n" << endl;
			}


		}//endif

		else if (a == 2) { //Kullanici Girisi
			int c = 0;			cout << "Kullanici olarak giris yaptiniz." << "\n" << endl;
			int kullanici = 0;
			while (kullanici == 0)
			{
				cout << "Ne yapmak istiyorsunuz ?" << endl;
				cout << "1.Kitaplari Goruntule" << endl;
				cout << "2.Kitap odunc al" << endl;
				cout << "3.Kitap teslim et" << endl;
				cout << "4.Sayfada ara" <<endl;
				cin >> c;
				cout << "------------------------" << endl;

				switch (c) {

				case 1: {
					int b;
					cout << "Tum kitaplar icin 1'i" << endl; //tum kitaolar� liste s�ras�na g�re basma
					cout << "Yazarlara gore aratmak icin 2'yi" << endl; //istenilen yazar�n kitaplar�n� basma
					cout << "Turlerine gore aratmak icin 3'u" << endl; //istenilen turun kitaplar�n� basma
					cout << "Basim tarihlerine gore aratmak icin 4 u" << endl; //kitab�n bas�m tarihine g�re basma
					cout << "  tuslayiniz." << "\n" << endl;

					cin >> b;
					system("cls");

					if (b == 1) {
						klist.print(); //kitap listin bas�lmas�
					}
					else if (b == 2) {
						
						klist.yazarPrint(ylist); // istenilen yazar�n kitaplar�n�n bas�lmas�
					}
					else if (b == 3) {
						klist.Siniflandir(); // istenilen turun kitaplar�n�n bas�lmas�
					}
					else if (b == 4)
					{
						klist.tarihPrint(); //bas�m tarihine g�re kitaplar�n bas�lmas�
					}
					else {	
						cout << "Dogru tuslama yapmadiniz." << endl;
					}
					break;
				}

				case 2: {
					klist.print();  // kitap listesinin bas�lmas�
					klist.oduncAl(); //�d�n� al�nacak kitab�n se�ilmesi
					cout << "------------------------" << endl;
					klist.print(); //kitap listin bas�lmas�
					break;
				}

				case 3: {
					klist.iadeEt(); // iade edilecek kitab�n se�ilmesi
					klist.print(); //kitaplistin bas�lmas�
					break;
				}
				
				case 4: {
					int i;
					klist.print();
					cout<<"Hangi kitapta aramak istiyorsun?"<<endl;
					cin>>i;
					kitap* temp = NULL;
					if(i > klist.elemanSayisi())
					{
						cout<<"Kitap bulunumadi !\n";
					}
					else
					{
						temp=klist.head;
						while(i>1)
						{
							temp=temp->next;
							i--;
						}
					}
					
					slist.ara(temp->kitapAdi); //sayfa i�inde girilen stringi arama fonksiyonu
					break;
				}

				default: { 
					cout << "Yanlis tuslama yaptiniz." << endl;
					break;
				}

				}//endswitch
				cout << "Ana menu icin 1 Kullanici menusu icin 0 girin" << endl;
				cin >> kullanici;
			}

		}

		else { 
			cout << "Yanlis tuslama yaptiniz." << endl;
		}

		cout<<"Cikmak icin 1 Ana menu icin 0 girin." << endl;
		cin >> sonlanma;
		system("cls");
	}
	
	system("PAUSE");
	return 0;
}


