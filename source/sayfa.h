//Bu�rahan Arslan
//330070
//HW-1
//sayfa.h

#ifndef SAYFA_H
#define SAYFA_H

#include <iostream>
#include <string>
#include "kitap.h"
#include <fstream>



class sayfa{
	
	public:
		sayfa *next; //sonraki sayfa
		class kitap* kitabi;	//sayfan�n ait oldu�u kitap
       
};

class sayfalist{
	public:
		sayfa *head;
		
		sayfalist();
		void ara(const string &kitapAdi); //sayfanin i�inde string arama fonksiyonu
		bool empty(); // list empty ?
		void dosyaAc(const string &kitapAdi); //kitab�n ad�nda dosya olu�turma fonksiyonu
			
};


sayfalist::sayfalist() {

	head = NULL;
	
}

bool sayfalist::empty() {
	
	return head == NULL;
}

void sayfalist::ara(const string &kitapAdi)
{
	string filename,aranan;
	filename =kitapAdi;
	filename += ".txt";
	
	cout<<"Sayfada aranacak kelimeyi girin."<<endl;
	
	cin.ignore();
	getline(cin,aranan);
	
	ifstream dosya;
	dosya.open(filename.c_str(),ios_base::app);
	
	if(dosya.fail())		//sayfan�n i�inde girilen stringin aranmas�
	{
		
		cout<<"Dosya acilamadi!"<<endl;
		
	}
	
	int curLine=0;
	string line;
	if(dosya.is_open())
	{
		
		while(getline(dosya,line))
		{
		
		curLine++;
			if (line.find(aranan, 0) != string::npos)
			{
				cout << "Bulundu: " << aranan <<" Bulundugu satir:\n"<<line<<endl; 
				cout << curLine <<". satir " << endl;
			}
		}
		
	}
	dosya.close();	
}

void sayfalist::dosyaAc(const string &kitapAdi){
	
	string filename;
	ofstream dosya;
	filename = kitapAdi;
	filename += ".txt";
							//kitab�n ad�nda txt olu�turulmas�
	dosya.open(filename.c_str(),ios_base::app);
	dosya.close();
	
}


#endif

