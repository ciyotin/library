//Buğrahan Arslan
//330070
//HW-1
//kitap.h

#ifndef KITAP_H
#define KITAP_H
#include <string>
#include "yazar.h"
#include "sayfa.h"


using namespace std;

class kitap {
public:
	string kitapAdi; 
	string kitapTuru;
	int basimTarihi;
	kitap *next;		// eklenen sonraki kitap
	kitap *nextBook;	// yazarin sonraki kitabi
	kitap *prevBook;	// yazarin onceki kitabi
	yazar *yazari;		// kitabin yazari
	bool alindi;		// kitabın ödünç alınıp alınmadığı
	class sayfa *sayfasi; // kitabın sayfası
	
};

class kitaplist {


public:

	kitap *head;

	kitaplist();
								// k: kitapadi  t: b.tarihi
	void   kitapEkle(const string &k, yazarlist &yList, sayfalist &sList, const int &t);//satırı kısaltmak gerek
	void   kitapSil();	 	
	void   Siniflandir(); //kitap türüne göre yazdırma
	void   print();	//kitap list sırasına göre yazdırma
	bool   empty(); // empty control
	int    elemanSayisi(); //kaç kitap olduğunu bulma
	void   oduncAl();	//kitabı ödünç alma fonksiyonu
	void   iadeEt(); //kitabı iade etme fonksiyon
	void   yazarPrint(yazarlist &yList);	//yazarın kitaplarını yazdırma fonksiyonu
	void   tarihPrint(); //kitapları basım tarihine göre yazdırma
};

kitaplist::kitaplist() {

	head = NULL;
}

bool kitaplist::empty() {
	return head == NULL;
}

void kitaplist::print() {

	if (empty())
	{
		cout << "Liste bos." << endl;
		return;
	}


	kitap* first = head;
	int a = 1;
	while (first != NULL)
	{
		cout << a << ".  " << first->kitapAdi << " ( ";
		cout	<< first->kitapTuru << " ) ( "<<first->basimTarihi<<" )"
		<<endl;
			if (first->alindi == 1)
			{
				cout << " (rafta degil...) ";
			}

			cout<< "\n" << endl;

		first = first->next;
		a++;
	}


}

void kitaplist::kitapEkle(const string &k,yazarlist &yList,sayfalist &sList,const int &t) { //satırı kısaltmak gerek

		//aynı türden fazla eklenmemesi için seçim yoluna gittik
	int i,genre;
	string g;
	cout << "Kitabin turunu seciniz" << endl
		<< "1. Bilim Kurgu\n2. Fantazi\n3. Biyografi\n4. Psikoloji\n"
		<< "5. Diger\n";
	cin >> genre;
	if (genre == 1)
	{
		g = "Bilim Kurgu";
	}
	else if (genre == 2)
	{
		g = "Fantazi";
	}
	else if (genre == 3)
	{
		g = "Biyografi";
	}
	else if (genre == 4)
	{
		g = "Psikoloji";
	}
	else if (genre == 5)
	{
		g = "Diger";
	}

	
	yList.print();
	cout << "Kitabin yazarinin numarasini girin." << endl;
	cin >> i;

	yazar* temp5 = yList.head;

	if (i <= yList.elemanSayisi())
	{
		while (i > 1)
		{
			//Yazar Node'unun secimi
			temp5 = temp5->next;
			i--;
		}
	}
	else
	{
		cout << "Yazar bulunamadi !" << endl;
		return;
	}

	kitap *v = new kitap;
	sayfa *y = new sayfa;
						//kitap ve sayfa nodelarının oluşturulması 
	v->kitapAdi = k;	//ve atamalar
	v->kitapTuru = g;
	v->basimTarihi = t;
	v->next = NULL;
	v->nextBook = NULL;
	v->alindi = 0;
	y->kitabi = v;
	v->sayfasi = y;
	
	if (head == NULL) //Listede kitap yoksa
	{
		head = v;
		v->prevBook = NULL;
		
		sList.head = y;			
		y->next = NULL;  //her eklenen sayfada sayfanın nextini null yap
	}
	else
	{
		kitap* first = head; //Next'i Null olana kadar ilerletiyoruz.
		while (first->next != NULL) first = first->next;
		first->next = v;
		
		sayfa* second = sList.head;
		while (second->next != NULL)
		{
			second = second->next; 
		}	
		second->next = y;
		y->next = NULL;

	}

	if (temp5->kitabi == NULL) //Yazarin kitabi yoksa
		
	{
		temp5->kitabi = v;
		v->yazari = temp5;
	}
	else
	{
		kitap *temp = temp5->kitabi;


		while (temp->nextBook != NULL)
		{
			temp = temp->nextBook;
		}
		temp->nextBook = v;
		v->prevBook = temp;
	}

}

int kitaplist::elemanSayisi() //kitap sayısının bulunması
{
	int a = 0;
	kitap* temp = head;
	while ((temp->next) != NULL)
	{
		temp = temp->next;
		a++;
	}
	a++;
	return a;
}

void  kitaplist::kitapSil()
{
	
	int i;
	cout << "Silinecek kitabin numarasini girin." << endl;

	cin >> i;

	if (i == 1)
	{
		kitap* temp = head;
		head = head->next;
		temp->yazari->kitabi = temp->nextBook;
		temp->nextBook->yazari = temp->yazari;
		temp->nextBook->prevBook = NULL;
		delete temp;

	}
	else if (i > (elemanSayisi()))	//Listede olmayan bir numaranin
	{
		cout << "Kitap bulunamadi !" << endl;//girilmesi durumunda...

	}
	else
	{
		kitap* temp = head;
		while (i - 1>1)
		{
			temp = temp->next;
			i--;
		}
		if (temp->next->prevBook != NULL)
		{
			if (temp->next->nextBook != NULL)
			{
				temp->next->nextBook->prevBook = temp->next->prevBook; 
				temp->next->prevBook->nextBook = temp->next->nextBook;
			}
			else
			{
				temp->next->prevBook->nextBook = NULL;
			}
			
			kitap *temp61 = temp->next;
			temp->next = temp->next->next;					
			delete temp61;

		}
		else
		{
			temp->next->yazari->kitabi = temp->next->nextBook;

			if (temp->next->nextBook != NULL)
			{
				temp->next->nextBook->yazari = temp->next->yazari;
				temp->next->nextBook->prevBook = NULL;

			}
			kitap *temp61 = temp->next;
			temp->next = temp->next->next;
			delete temp61;
		}


		//delete temp->next;
	}



}

void   kitaplist::oduncAl()
{
	if (empty())
	{
		cout << "\nOdunc alinacak kitap yok ! \n";
		return;
	}
	int i;
	cout << "Odunc alinacak kitabin numarasini girin." << endl;
	cin >> i;
	kitap* temp = NULL;

	

	if (i > (elemanSayisi()))		//Listede olmayan bir numaranin
	{
		cout << "Kitap bulunamadi !" << endl;//girilmesi durumunda...

	}
	else
	{
		temp = head;
		while (i>1)
		{
			temp = temp->next;
			i--;
		}
		
	}

	if (temp->alindi == 1)
	{
		cout << "Kitap rafta degil" << endl;
	}
	else
	{
		//temp->kitapAdi = temp->kitapAdi + " ( rafta degil. )"; 
		//kitabın adını değiştirmemek için
		temp->alindi = 1;
	}

}

void   kitaplist::iadeEt()
{						// alınmış bir kitabın iade edilmesi
	kitap* iade = NULL;
	kitap* first = head;
	int a = 1;
	while (first != NULL)
	{
		if (first->alindi == 1)
		{
			cout << a << ".  " << first->kitapAdi << " ( "
			<< first->kitapTuru << " )"<< "\n" << endl;

			a++;
		}
		first = first->next;
	}
	if (a == 1)
	{
		cout << "iade edilecek kitap yok." << endl;
		return;
	}

	
	cout << "iade etmek istediginiz kitabin numarasini girin." << endl;
	int i;
	cin >> i;

	first = head;
	
	while (i < a)
	{
		if (first->alindi == 1)
		{
			a--;
			iade = first;
		}
		first = first->next;
	}

	iade->alindi = 0;

	cout << "kitabiniz basariyla iade edilmistir.\t" << endl;

}

void   kitaplist::Siniflandir()		
{				//türlere göre kitap basılması		
	int tur;
	
	cout<<"Hangi turu gormek istiyorsunuz?"<<endl	
		<<"1. Bilim Kurgu"<<endl
		<<"2. Fantazi"<<endl
		<<"3. Biyografi"<<endl
		<<"4. Psikoloji"<<endl
		<<"5. Diger"<<endl;
	cin >> tur;
	int i = 0;

	kitap* temp = head;
	if (tur == 1 )
	{
		while (temp != NULL)
		{
			if (temp->kitapTuru == "Bilim Kurgu")
			{
				cout << temp->kitapAdi << endl;
				i++;
			}
			temp = temp->next;
		}
		if (i == 0)
		{
			cout << "Bu turden hic kitabimiz yok." << endl;
		}
	}
	else if (tur == 2)
	{
		while (temp != NULL)
		{
			if (temp->kitapTuru == "Fantazi")
			{
				cout << temp->kitapAdi << endl;
				i++;
			}
			temp = temp->next;
		}
		if (i == 0)
		{
			cout << "Bu turden hic kitabimiz yok." << endl;
		}
	}
	else if (tur == 3)
	{
		while (temp != NULL)
		{
			if (temp->kitapTuru == "Biyografi")
			{
				cout << temp->kitapAdi << endl;
				i++;
			}
			temp = temp->next;
		}
		if (i == 0)
		{
			cout << "Bu turden hic kitabimiz yok." << endl;
		}
	}
	else if (tur == 4)
	{
		while (temp != NULL)
		{
			if (temp->kitapTuru == "Psikoloji")
			{
				cout << temp->kitapAdi << endl;
				i++;
			}
			temp = temp->next;
		}
		if (i == 0)
		{
			cout << "Bu turden hic kitabimiz yok." << endl;
		}
	}
	else if (tur == 5)
	{
		while (temp != NULL)
		{
			if (temp->kitapTuru == "Diger")
			{
				cout << temp->kitapAdi << endl;
				i++;
			}
			temp = temp->next;
		}
		if (i == 0)
		{
			cout << "Bu turden hic kitabimiz yok." << endl;
		}
	}
	else
	{
		cout << "Yanlis deger girdiniz." << endl;
	}
}

void  kitaplist::yazarPrint(yazarlist &yList)
{
		//seçilen yazarın kitaplarının basılması
	cout << "Kacinci yazarin kitaplarini gormek istiyorsunuz?" << endl;

	yazar* temp = yList.head;
	int i = 1,k=0,a;

	while (temp != NULL)
	{
		cout << i << ". " << temp->yazaradi << endl;
		temp = temp->next;
		i++;
	}
	cin >> a;
	
	temp = yList.head;
	
	while (a > 1)
	{
		temp = temp->next;
		a--;
	}
	
	kitap* temp2 = temp->kitabi;

	while (temp2 != NULL)
	{
		cout << temp2->kitapAdi << endl;
		temp2 = temp2->nextBook;
		k++;
	}
	if (k == 0)
	{
		cout << "Sectiginiz yazarin elimizde hic kitabi yok." << endl;
	}
		
}

void   kitaplist::tarihPrint()
{				//basım tarihine göre küçükten büyüğe kitapların basılması
	if (head == NULL)
	{
		cout << "Hic kitap yok !" << endl;
		return;
	}
	
	kitap* temp=head;
	int b = temp->basimTarihi;
	int k = temp->basimTarihi;
	temp = temp->next;
	while (temp != NULL)
	{
		if (temp->basimTarihi < k)
		{
			k = temp->basimTarihi;
		}
		if (temp->basimTarihi > b)
		{
			b = temp->basimTarihi;
		}
		temp = temp->next;
	}
	temp = head;
	for (int i = k; i <= b; i++)
	{
		while (temp != NULL)
		{
			if (temp->basimTarihi == i)
			{
				cout << temp->kitapAdi << "  ( " << temp->kitapTuru << " ) ( " << temp->basimTarihi << " ) " << endl;
			}
			temp = temp->next;
		}
		temp = head;
		
	}
}


#endif
